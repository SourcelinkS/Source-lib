/*
BeagleBoneBlack的ADC函数
*/
#include "sl_adc.h"
#include "bbb_adc.h"
DEBUG_SET_LEVEL(DEBUG_LEVEL_ERR);

/*
初始化
*/
//echo BB-ADC > /sys/devices/bone_capemgr.9/slots
//echo -BB-ADC > /sys/devices/bone_capemgr.9/slots
EXPORT int bbb_adc_install()
{
	return adc_set("/sys/devices/bone_capemgr.9/slots","BB-ADC");
}
EXPORT int bbb_adc_uninstall()
{
	return adc_set("/sys/devices/bone_capemgr.9/slots", "-BB-ADC");
}
/*
读取
*/
// "/sys/bus/iio/devices/iio:device0/in-voltage%d_raw",pin
EXPORT int bbb_adc_read(int pin)
{
	char filename[64];
	sprintf(filename, "/sys/bus/iio/device/iio:device0/in_voltage%d_raw", pin)
		return get_adc_value(const *filename);
}