
#ifndef BBB_ADC_H_
#define BBB_ADC_H_

#ifdef __cplusplus
extern "C" {
#endif
	extern int bbb_adc_install();
	extern int bbb_adc_uninstall();
	extern int bbb_adc_read(int pin);
#ifdef __cplusplus
}
#endif


#endif